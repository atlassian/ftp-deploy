# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.7.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.7.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.6.1

- patch: Internal maintenance: Refactor tests.

## 0.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.5.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit to fix vulnerability with certify CVE-2023-37920.

## 0.4.1

- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.

## 0.4.0

- minor: Add support of the custom set command arguments.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update docker images and internal pipe versions.
- patch: Internal maintenance: update packages in Dockerfile.
- patch: Internal maintenance: update release process.

## 0.3.7

- patch: Internal maintenance: Add required tags for test infra resources.
- patch: Internal maintenance: Refactor Dockerfile to fix hadolint error.

## 0.3.6

- patch: Add examples to the Readme.

## 0.3.5

- patch: Internal maintenance: fix readme

## 0.3.4

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.3.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.2

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.3.0

- minor: Added DELETE_FLAG variable: Made --delete-first optional.

## 0.2.3

- patch: Add possibility to use extra arguments

## 0.2.2

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.2.1

- patch: Documentaion update

## 0.2.0

- minor: Disabled ssl when initiating the connection which caused some issues when running the pipe

## 0.1.1

- patch: Update pipes bash toolkit version.

## 0.1.0

- minor: Initial release
