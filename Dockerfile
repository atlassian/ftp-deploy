FROM alpine:3.17

RUN apk add --update --no-cache \
    bash=5.2.15-r0 \
    lftp=4.9.2-r4 && \
    wget --no-verbose -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
