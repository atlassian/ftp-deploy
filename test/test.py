import os
from pathlib import Path

from bitbucket_pipes_toolkit.test import PipeTestCase


class FTPDeployTestCase(PipeTestCase):
    dirname = os.path.dirname(__file__)
    test_image_name = 'test-image'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # get Variables
        cls.user = 'foo'
        cls.password = 'bar'
        cls.local_path = 'test/test-files'
        cls.remote_path = '/tmp'

        cls.image = cls.docker_client.images.build(
            path=cls.dirname, dockerfile=os.path.join(cls.dirname, 'Dockerfile'), tag=cls.test_image_name)

    def setUp(self):
        self.maxDiff = None
        self.api_client = self.docker_client.api
        self.container = self.docker_client.containers.run(
            self.test_image_name, detach=True)

        self.container_ip = self.api_client.inspect_container(self.container.id)['NetworkSettings']['IPAddress']

    def tearDown(self):
        self.container.kill()

    def test_fail_no_parameters(self):
        result = self.run_container()
        self.assertIn('USER variable missing', result)

    def test_ftp_deployment_successful(self):
        result = self.run_container(environment={
            'USER': self.user,
            'PASSWORD': self.password,
            'SERVER': self.container_ip,
            'REMOTE_PATH': self.remote_path,
            'BITBUCKET_CLONE_DIR': os.path.join(os.getcwd(), 'test')
        })

        self.assertRegex(result, r'✔ Deployment finished.')

    def test_ftp_deployment_successful_custom_set_command(self):
        result = self.run_container(environment={
            'USER': self.user,
            'PASSWORD': self.password,
            'SERVER': self.container_ip,
            'REMOTE_PATH': self.remote_path,
            'SET_ARGS': "ssl:verify-certificate false",
            'BITBUCKET_CLONE_DIR': os.path.join(os.getcwd(), 'test')
        })

        self.assertRegex(result, r'✔ Deployment finished.')

    def test_file_are_present_after_deploy(self):
        result = self.run_container(environment={
            'USER': self.user,
            'PASSWORD': self.password,
            'SERVER': self.container_ip,
            'REMOTE_PATH': self.remote_path,
            'LOCAL_PATH': self.local_path,
            'DEBUG': 'true'
        }, stderr=True)

        self.assertIn('Finished transfer `hello.txt', result)
        self.assertRegex(result, r'✔ Deployment finished.')

    def test_ftp_deployment_exclude_file(self):
        extra_args = "--exclude=hello.txt"

        result = self.run_container(environment={
            'USER': self.user,
            'PASSWORD': self.password,
            'SERVER': self.container_ip,
            'REMOTE_PATH': self.remote_path,
            'LOCAL_PATH': self.local_path,
            'EXTRA_ARGS': extra_args
        }, stderr=True)

        self.assertNotIn('Finished transfer `hello.txt', result)
        self.assertRegex(result, r'✔ Deployment finished.')

    def test_file_present_if_delete_flag_is_false(self):
        # create empty test file
        test_file = "test_file.txt"
        Path(Path.cwd() / self.local_path / test_file).touch()

        result = self.run_container(environment={
            'USER': self.user,
            'PASSWORD': self.password,
            'SERVER': self.container_ip,
            'REMOTE_PATH': self.remote_path,
            'LOCAL_PATH': self.local_path,
        }, stderr=True)

        self.assertRegex(result, r'✔ Deployment finished.')

        # remove test file from local directory
        Path(Path.cwd() / self.local_path / test_file).unlink()

        # run with DELETE_FLAG = false
        result = self.run_container(environment={
            'USER': self.user,
            'PASSWORD': self.password,
            'SERVER': self.container_ip,
            'REMOTE_PATH': self.remote_path,
            'LOCAL_PATH': self.local_path,
            'DELETE_FLAG': 'false'
        }, stderr=True)

        list_files_in_container_command = f'ls /home/{self.user}/{self.remote_path}'

        assert test_file in str(self.container.exec_run(list_files_in_container_command)[1].decode())
        self.assertRegex(result, r'✔ Deployment finished.')
